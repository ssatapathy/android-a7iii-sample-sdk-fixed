package com.example.sony.cameraremote.utils;

import android.util.Log;

import java.util.ArrayList;

public class Logger {

    private static int LOG_LINE_LEN = 1000;
    public static void log (String TAG, String msg) {
        if (msg.length() <= LOG_LINE_LEN) {
            Log.v(TAG, msg);
        } else {
            int chunkCount = msg.length() / LOG_LINE_LEN;
            for (int i = 0; i <= chunkCount; i++) {
                int max = LOG_LINE_LEN * (i + 1);
                if (max >= msg.length()) {
                    Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + msg.substring(LOG_LINE_LEN * i));
                } else {
                    Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + msg.substring(LOG_LINE_LEN * i, max));
                }
            }
        }
    }
}
